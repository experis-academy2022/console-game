﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment4.helpers.characters
{
    public class Ranger : Character
    {
        public Ranger()
        {
            Name = "ranger";
            BaseStrength = 1;
            BaseDexterity = 7;
            BaseIntelligence = 1;
            TotalStrength = BaseStrength;
            TotalDexterity = BaseDexterity;
            TotalIntelligence = BaseIntelligence;
        }

    }
}
