﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Text;
using System.Threading.Tasks;

namespace assignment4.helpers.characters
{
    public class Mage : Character
    {
        
        public Mage()
        {
            Name = "mage";
            BaseStrength = 1;
            BaseDexterity = 1;
            BaseIntelligence = 8;
            TotalStrength = BaseStrength;
            TotalDexterity = BaseDexterity;
            TotalIntelligence = BaseIntelligence;

        }

    }
}
