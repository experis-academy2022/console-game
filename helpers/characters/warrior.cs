﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment4.helpers.characters
{
    public class Warrior : Character
    {
        public Warrior()
        {
            Name = "warrior";
            BaseStrength = 5;
            BaseDexterity = 2;
            BaseIntelligence = 1;
            TotalStrength = BaseStrength;
            TotalDexterity = BaseDexterity;
            TotalIntelligence = BaseIntelligence;
        }
      
    }
}
