﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment4.helpers.characters
{
    public class Rogue : Character
    {
        public Rogue()
        {
            Name = "rogue";
            BaseStrength = 2;
            BaseDexterity = 6;
            BaseIntelligence = 1;
            TotalStrength = BaseStrength;
            TotalDexterity = BaseDexterity;
            TotalIntelligence = BaseIntelligence;
        }

    }
}
