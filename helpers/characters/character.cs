﻿using assignment4.helpers.equipments;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace assignment4.helpers.characters
{
    public class Character
    {

        public Character()
        {
            Level = 1;
            EquipmentsOfCharacter = new List<Equipment>();
        }

        // name
        public string Name { get ; set; }
        // level
        public int Level { get; set; }

        // base primary attributes (strength, dexterity, intelligence)
        public int BaseStrength { get; set; }
        public int BaseDexterity { get; set; }
        public int BaseIntelligence { get; set; }


        // >strength-unit: warrior damage increases 1%
        // >dexterity-unit: ranger and rogue damage increases 1%
        // >intelligence-unit: mage damage increases 1%

        // total primary attributes (strength, dexterity, intelligence)
        // >Total attribute = attributes from level + attributes from all equipped armor
        public int TotalStrength { get; set; }
        public int TotalDexterity { get; set; }
        public int TotalIntelligence { get; set; }

        // Character damage = Weapon DPS * (1 + TotalPrimaryAttribute/100)
        public double DPS_character { get; set; }

        // theCharactersEquipment
        public List<Equipment> EquipmentsOfCharacter { get; set; }

        // stats display with string builder:
        // • Character name
        // • Character level
        // • Strength (total)
        // • Dexterity (total)
        // • Intelligence (total)
        // • Damage
        
        public int stats()
        {
            Console.WriteLine("Name: " + this.Name + "\nLevel: " + this.Level + "\nStrength: " + this.TotalStrength + "\nDexterity: "
                + this.TotalDexterity + "\nIntelligence: " + this.TotalIntelligence);
            return 1;
        }


        // when created, given a name, and level: 1 and base attribute values
        


    }
}
