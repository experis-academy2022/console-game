﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment4.helpers.equipments
{
    public class Armor : Equipment
    {
        public Armor(string name, int levelToEquip)
        {
            Name = name; // cloth, leather, mail, plate
            LevelToEquip = levelToEquip;
        }
    }
}
