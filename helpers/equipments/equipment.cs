﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment4.helpers.equipments
{
    public class Equipment
    {
        public Equipment()
        {

        }

        // name
        public string Name { get; set; }
        // required level to equip the equipment
        public int LevelToEquip { get; set; }
        // slot in which the equipment is equipped:
        // > head (armor): 1, body (armor): 2, legs (armor): 3, weapon (weapon): 4
        public int Slot { get; set; }
    }
}
