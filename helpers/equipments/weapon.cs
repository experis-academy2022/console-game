﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace assignment4.helpers.equipments
{
    public class Weapon : Equipment
    {
        public Weapon(string name, int levelToEquip)
        {
            Name = name;
            LevelToEquip = levelToEquip;
        }
        // axes, bows, daggers, hammers, staffs, swords, wands
        // base damage
        public double BaseDamage { get; set; }
        // attacks per second
        public double AttacksPerSecond { get; set; }
        // DPS (damage per second) = damage * attack speed
        public double DPS { get; set; }
    }
}
