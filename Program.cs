﻿

// See https://aka.ms/new-console-template for more information
using assignment4.helpers.characters;
using assignment4.helpers.equipments;

// console app 

// print instructions such as commands 
// -choose a character -> A, B, C, D
Console.WriteLine("Hello! Choose a fighter: \n A - mage \n B - ranger \n C - rogue \n D - warrior");

// user input
string input1 = Console.ReadLine();

// create a character
Character hero = new Character();

// check what userinput means: choosing character
if (input1 == "A") {
    Mage mage = new Mage();
    hero = mage;
} else if (input1 == "B") {
    Ranger ranger = new Ranger();
    hero = ranger;
} else if (input1 == "C") {
    Rogue rogue = new Rogue();
    hero = rogue;
} else if (input1 == "D") {
    Warrior warrior = new Warrior();
    hero = warrior;
} else {
    Console.WriteLine("throw error");
}


// while loop to run program till user closes window
while (true)
{

    // intructions:
    Console.WriteLine("You have chosen: " + hero.Name + "\n To level up press L \n To add an equipment press E \n" +
    " To delete an equipment press D \n To see fighter stats press P");

    // user input
    string input2 = Console.ReadLine();


    // create equipments
    Equipment equipment = new Equipment();
    Weapon axe = new Weapon("axe", 8);
    Weapon bow = new Weapon("bow", 3);
    Weapon dagger = new Weapon("dagger", 10);
    Weapon hammer = new Weapon("hammer", 11);
    Weapon staff = new Weapon("staff", 5);
    Weapon sword = new Weapon("sword", 6);
    Weapon wand = new Weapon("wand", 7);
    Armor cloth = new Armor("cloth", 1);
    Armor leather = new Armor("leather",9);
    Armor mail = new Armor("mail", 2);
    Armor plate = new Armor("plate", 4);



    // check what userinput means: action


    // level up
    if (input2 == "L")
    {
        hero.Level += 1;
        Console.WriteLine("fighter level: " + hero.Level);
        if (hero.Name == "mage")
        {
            hero.TotalStrength += 1;
            hero.TotalDexterity += 1;
            hero.TotalIntelligence += 5;
        } else if (hero.Name == "ranger")
        {
            hero.TotalStrength += 1;
            hero.TotalDexterity += 5;
            hero.TotalIntelligence += 1;
        } else if (hero.Name == "rogue")
        {
            hero.TotalStrength += 1;
            hero.TotalDexterity += 4;
            hero.TotalIntelligence += 1;
        } else if (hero.Name == "warrior")
        {
            hero.TotalStrength += 3;
            hero.TotalDexterity += 2;
            hero.TotalIntelligence += 1;
        }
    }
    // equipment
    // -equipment add/delete, including options to add armor in different slots -> E/D -> 1, 2, 3, 
    else if (input2 == "E")
    {
        Console.WriteLine("Equipments:\nArmors:\n-cloth (1)\n-leather (2)\n-mail (3)\n-plate (4)\n" +
            "Weapons:\n-axe (5)\n-bow (6)\n-dagger (7)\n-hammer (8)\n-staff (9)\n-sword (10)\n-wand (11)");
        Console.WriteLine("Choose an equipment to add (number): ");
        string input3 = Console.ReadLine();

        if (input3 == "1")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 1, Required Character: mage");
            if (hero.Level >= 1 && hero.Name == "mage")
            {
                hero.EquipmentsOfCharacter.Add(cloth);
                hero.TotalStrength += 1;
                hero.TotalDexterity += 1;
                hero.TotalIntelligence += 1;
                Console.WriteLine("What do you want to armor?\nhead (1), body (2), legs (3)");
                string input4 = Console.ReadLine();
                cloth.Slot = Convert.ToInt32(input4);
                Console.WriteLine(cloth.Name + " added succesfully!");
            } else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid armor exception
            }
            
        } else if (input3 == "2")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 9, Required Character: ranger or rogue");
            if (hero.Level >= 9 && (hero.Name == "ranger" || hero.Name == "rogue")) {
                hero.EquipmentsOfCharacter.Add(leather);
                hero.TotalStrength += 2;
                hero.TotalDexterity += 4;
                hero.TotalIntelligence += 1;
                Console.WriteLine("What do you want to armor?\nhead (1), body (2), legs (3)");
                string input4 = Console.ReadLine();
                leather.Slot = Convert.ToInt32(input4);
                Console.WriteLine(leather.Name + " added succesfully!");
            } else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid armor exception
            }

        } else if (input3 == "3")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 2, Required Character: ranger, rogue or warrior");
            if (hero.Level >= 2 && (hero.Name == "ranger" || hero.Name == "rogue" || hero.Name == "warrior"))
            {
                hero.EquipmentsOfCharacter.Add(mail);
                hero.TotalStrength += 1;
                hero.TotalDexterity += 1;
                hero.TotalIntelligence += 3;
                Console.WriteLine("What do you want to armor?\nhead (1), body (2), legs (3)");
                string input4 = Console.ReadLine();
                mail.Slot = Convert.ToInt32(input4);
                Console.WriteLine(mail.Name + " added succesfully!");
            } else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid armor exception
            }

        } else if (input3 == "4")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 4, Required Character: warrior");
            if (hero.Level >= 4 && hero.Name == "warrior")
            {
                hero.EquipmentsOfCharacter.Add(plate);
                hero.TotalStrength += 1;
                hero.TotalDexterity += 5;
                hero.TotalIntelligence += 1;
                Console.WriteLine("What do you want to armor?\nhead (1), body (2), legs (3)");
                string input4 = Console.ReadLine();
                plate.Slot = Convert.ToInt32(input4);
                Console.WriteLine(plate.Name + " added succesfully!");
            } else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid armor exception
            }

        } else if (input3 == "5")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 8, Required Character: warrior");
            if (hero.Level >= 8 && hero.Name == "warrior")
            {
                hero.EquipmentsOfCharacter.Add(axe);
                hero.TotalStrength += 7;
                hero.TotalDexterity += 1;
                hero.TotalIntelligence += 1;
                axe.Slot = 4;
                Console.WriteLine(axe.Name + " added succesfully!");
            } else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid weapon exception
            }

        } else if (input3 == "6")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 3, Required Character: ranger");
            if (hero.Level >= 3 && hero.Name == "ranger")
            {
                hero.EquipmentsOfCharacter.Add(bow);
                hero.TotalStrength += 1;
                hero.TotalDexterity += 1;
                hero.TotalIntelligence += 1;
                bow.Slot = 4;
                Console.WriteLine(bow.Name + " added succesfully!");
            } else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid weapon exception
            }

        } else if (input3 == "7")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 10, Required Character: rogue");
            if (hero.Level >= 10 && hero.Name == "rogue")
            {
                hero.EquipmentsOfCharacter.Add(dagger);
                hero.TotalStrength += 10;
                hero.TotalDexterity += 7;
                hero.TotalIntelligence += 1;
                dagger.Slot = 4;
                Console.WriteLine(dagger.Name + " added succesfully!");
            } else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid weapon exception
            }

        } else if (input3 == "8")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 11, Required Character: warrior");
            if (hero.Level >= 11 && hero.Name == "warrior")
            {
                hero.EquipmentsOfCharacter.Add(hammer);
                hero.TotalStrength += 1;
                hero.TotalDexterity += 4;
                hero.TotalIntelligence += 8;
                hammer.Slot = 4;
                Console.WriteLine(hammer.Name + " added succesfully!");
            } else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid weapon exception
            }
        } else if (input3 == "9")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 5, Required Character: mage");
            if (hero.Level >= 5 && hero.Name == "mage")
            {
                hero.EquipmentsOfCharacter.Add(staff);
                hero.TotalStrength += 11;
                hero.TotalDexterity += 11;
                hero.TotalIntelligence += 10;
                staff.Slot = 4;
                Console.WriteLine(staff.Name + " added succesfully!");
            }
            else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid weapon exception
            }
        } else if (input3 == "10")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 6, Required Character: rogue or warrior");
            if (hero.Level >= 6 && (hero.Name == "rogue" || hero.Name == "warrior"))
            {
                hero.EquipmentsOfCharacter.Add(sword);
                hero.TotalStrength += 7;
                hero.TotalDexterity += 1;
                hero.TotalIntelligence += 2;
                sword.Slot = 4;
                Console.WriteLine(sword.Name + " added succesfully!");
            } else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid weapon exception
            }
        } else if (input3 == "11")
        {
            // requirements to equip: level and character
            Console.WriteLine("Required level: 7, Required Character: mage");
            if (hero.Level >= 7 && hero.Name == "mage")
            {
                hero.EquipmentsOfCharacter.Add(wand);
                hero.TotalStrength += 1;
                hero.TotalDexterity += 1;
                hero.TotalIntelligence += 9;
                wand.Slot = 4;
                Console.WriteLine(wand.Name + " added succesfully!");
            } else
            {
                Console.WriteLine("Requirements do not match!");
                // throw invalid weapon exception
            }
        }


    }
    else if (input2 == "D")
    {
        Console.WriteLine("Equipments:\nArmors:\n-cloth (1)\n-leather (2)\n-mail (3)\n-plate (4)\n" +
            "Weapons:\n-axe (5)\n-bow (6)\n-dagger (7)\n-hammer (8)\n-staff (9)\n-sword (10)\n-wand (11)");
        Console.WriteLine("Choose an equipment to remove (number): ");
        string input3 = Console.ReadLine();

        if (input3 == "1")
        {
            hero.EquipmentsOfCharacter.Remove(cloth);
            hero.TotalStrength -= 1;
            hero.TotalDexterity -= 1;
            hero.TotalIntelligence -= 1;
            // remember to delete also a slot
            cloth.Slot = 0;
            Console.WriteLine(cloth.Name + " removed succesfully!");
            
        }
        else if (input3 == "2")
        {
            hero.EquipmentsOfCharacter.Remove(leather);
            hero.TotalStrength -= 2;
            hero.TotalDexterity -= 4;
            hero.TotalIntelligence -= 1;
            // remember to delete also a slot
            leather.Slot = 0;
            Console.WriteLine(leather.Name + " removed succesfully!");
        }
        else if (input3 == "3")
        {
            hero.EquipmentsOfCharacter.Remove(mail);
            hero.TotalStrength -= 1;
            hero.TotalDexterity -= 1;
            hero.TotalIntelligence -= 3;
            // remember to delete also a slot
            mail.Slot = 0;
            Console.WriteLine(mail.Name + " removed succesfully!");
        }
        else if (input3 == "4")
        {
            hero.EquipmentsOfCharacter.Remove(plate);
            hero.TotalStrength -= 1;
            hero.TotalDexterity -= 5;
            hero.TotalIntelligence -= 1;
            // remember to delete also a slot
            plate.Slot = 0;
            Console.WriteLine(plate.Name + " removed succesfully!");
        }
        else if (input3 == "5")
        {
            hero.EquipmentsOfCharacter.Remove(axe);
            hero.TotalStrength -= 7;
            hero.TotalDexterity -= 1;
            hero.TotalIntelligence -= 1;
            // remember to delete also a slot
            axe.Slot = 0;
            Console.WriteLine(axe.Name + " removed succesfully!");
        }
        else if (input3 == "6")
        {
            hero.EquipmentsOfCharacter.Remove(bow);
            hero.TotalStrength -= 1;
            hero.TotalDexterity -= 1;
            hero.TotalIntelligence -= 1;
            // remember to delete also a slot
            bow.Slot = 0;
            Console.WriteLine(bow.Name + " removed succesfully!");
        }
        else if (input3 == "7")
        {
            hero.EquipmentsOfCharacter.Remove(dagger);
            hero.TotalStrength -= 10;
            hero.TotalDexterity -= 7;
            hero.TotalIntelligence -= 1;
            // remember to delete also a slot
            dagger.Slot = 0;
            Console.WriteLine(dagger.Name + " removed succesfully!");
        }
        else if (input3 == "8")
        {
            hero.EquipmentsOfCharacter.Remove(hammer);
            hero.TotalStrength -= 1;
            hero.TotalDexterity -= 4;
            hero.TotalIntelligence -= 8;
            // remember to delete also a slot
            hammer.Slot = 0;
            Console.WriteLine(hammer.Name + " removed succesfully!");
        }
        else if (input3 == "9")
        {
            hero.EquipmentsOfCharacter.Remove(staff);
            hero.TotalStrength -= 11;
            hero.TotalDexterity -= 11;
            hero.TotalIntelligence -= 10;
            // remember to delete also a slot
            staff.Slot = 0;
            Console.WriteLine(staff.Name + " removed succesfully!");
        }
        else if (input3 == "10")
        {
            hero.EquipmentsOfCharacter.Remove(sword);
            hero.TotalStrength -= 7;
            hero.TotalDexterity -= 1;
            hero.TotalIntelligence -= 2;
            // remember to delete also a slot
            sword.Slot = 0;
            Console.WriteLine(sword.Name + " removed succesfully!");
        }
        else if (input3 == "11")
        {
            hero.EquipmentsOfCharacter.Remove(wand);
            hero.TotalStrength -= 1;
            hero.TotalDexterity -= 1;
            hero.TotalIntelligence -= 9;
            // remember to delete also a slot
            wand.Slot = 0;
            Console.WriteLine(wand.Name + " removed succesfully!");
        }

    }

    // show stats of character 

    else if (input2 == "P")
    {
        hero.stats();
    }

}






