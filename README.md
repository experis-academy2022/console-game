# Console App Game

This project is a console app game where user can choose a character, arm it and fight with it. This is a first back-end project with C# and Visual Studio and was made with object oriented programming.
<br>
<br>

## Features
- Choose a character
- Arm the chosen character with armors and weapons, each item requires a spesific character and level
- Level up
- Show statistics of the character
<br>

## Installation
You need to clone or download the project to your computer.

To clone the project run in console:
```
git clone https://gitlab.com/noroff4/movies-assignment.git

```
Open solution in Visual Studio.
<br>

## Usage
After installation steps, you can run the application and use application in a console.
<br>

## Contributor
- Marie Puhakka https://gitlab.com/mariesusan
<br>


## License
[MIT](https://choosealisence.com/licenses/mit/)







